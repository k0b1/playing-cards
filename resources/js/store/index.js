import api from "./../api/api";

export default {
  state: {
    gameStart: {
      first_player: "",
      second_player: "",
      first_player_cards: {},
      cards_on_table: [],
      second_player_cards: {},
      cards_on_table: [],
      userID: 0,
      gameID: 0
    },
    playerPoints: 0,
    takenCards: "",
    gameResult: 0,
    additionalPoints: 0,
    amINext: false,
    gameResultTotal: { playerOnePoints: 0, playerTwoPoints: 0 },
    matchResultPlayer1: 0,
    matchResultPlayer2: 0,
    cardsForNewGame: {
      first_player: "",
      second_player: "",
      first_player_cards: {},
      second_player_cards: {},
      cards_on_table: [],
      userID: 0,
      gameID: 0
    },
    finalResult: [],
    whoPlaysFirst: 0
  },
  getters: {
    getGameStart(state) {
      return state.gameStart;
    },
    getPlayerPoints(state) {
      return state.playerPoints;
    },
    getTakenCards(state) {
      return state.takenCards;
    },
    getGameResult(state) {
      return state.gameResult;
    },
    getMatchResultPlayer1(state) {
      return state.matchResultPlayer1;
    },
    getMatchResultPlayer2(state) {
      return state.matchResultPlayer2;
    },
    getGameTotalResult(state) {
      return state.gameResultTotal;
    },
    getFinalResult(state) {
      return state.finalResult;
    },
    getAdditionalPoints(state) {
      return state.additionalPoints;
    },
    getAmINext(state) {
      return state.amINext;
    },
    getCardsForNewGame(state) {
      return state.cardsForNewGame;
    },
    getWhoPlaysFirst(state) {
      return state.whoPlaysFirst;
    }
  },
  actions: {
    setGamePoints({ commit }, value) {
      commit("addPointsToResult", value);
    },
    setTakenCards({ commit }, value) {
      commit("addCardToTakenCards", value);
    },
    increaseAdditionalPoints({ commit }, value) {
      commit("sumAdditionaPoints", value);
    },
    apiActionIncreaseAdditionalPoints({ commit }, gameData) {
      api.increaseAdditionalPoints
        .bind(
          {},
          gameData.player1,
          gameData.player2,
          gameData.current_player,
          gameData.additionalPoint
        )()
        .then(response => {
          commit("setNewAdditionalPoints", response);
        });
    },
    apiActionGetAdditionalPoints({ commit }, gameData) {
      api.getAdditionalPoints
        .bind({}, gameData.player1, gameData.player2, gameData.current_player)()
        .then(response => {
          commit("setNewAdditionalPoints", response);
        });
    },
    apiActionDisplayGameResult({ commit }, gameDataObject) {
      api.totalResultForPlayers
        .bind(
          {},
          gameDataObject.playerOne,
          gameDataObject.playerTwo,
          gameDataObject.current_player
        )()
        .then(response => {
          commit("setTotalGameResult", {
            playerOne: response.data.player1_poens,
            playerTwo: response.data.player2_poens
          });
        });
    },
    apiActionSetNewGame({ commit }, newGameStarted) {
      api.startGame
        .bind({}, newGameStarted.playerOne, newGameStarted.playerTwo)()
        .then(response => {
          // console.log("response", response);
          commit("startNewGame", {
            playerOne: response.first_player,
            playerTwo: response.second_player,
            playerOneCards: response.first_player_cards,
            playerTwoCards: response.second_player_cards,
            cardsOnTable: response.cards_on_table,
            userID: response.userID,
            gameID: response.gameID
          });
        });
    },
    apiActionGetMatchResults({ commit }, gameData) {
      api.getMatchResultFromDB
        .bind({}, gameData.player1, gameData.player2, gameData.current_player)()
        .then(response => {
          commit("getMatchResults", response);
        });
    },
    apiActionGetFinalResult({ commit }, gameData) {
      api.getGameResultFromDB
        .bind(
          {},
          gameData.playerOne,
          gameData.playerTwo,
          gameData.currentPlayer
        )()
        .then(res => {
          console.log("misko", res.data);

          commit("getFinalResultAPI", res.data);
        });
    },
    toggleGameStart({ commit }, value) {
      commit("changeGameStart", value);
    },
    changeAmINext({ commit }, value) {
      commit("setAmINext", value);
    }
  },
  mutations: {
    addPointsToResult(state, value) {
      state.gameResult = value;
    },
    addCardToTakenCards(state, value) {
      state.takenCards = value;
    },
    sumAdditionaPoints(state, value) {
      state.additionalPoints = value;
    },
    setNewAdditionalPoints(state, value) {
      //   console.log("value in vuex", value);
      state.additionalPoints = value.data[0].player_one_additional_points
        ? value.data[0].player_one_additional_points
        : value.data[0].player_two_additional_points;
    },
    changeGameStart(state, value) {
      console.log("previous start", value);
      state.gameStart.first_player = value.first_player;
      state.gameStart.second_player = value.second_player;
      state.gameStart.cards_on_table = value.cards_on_table;
      state.gameStart.first_player_cards = value.first_player_cards;
      state.gameStart.second_player_cards = value.second_player_cards;
      state.gameStart.userID = value.userID;
      state.gameStart.gameID = value.gameID;
    },
    setAmINext(state, value) {
      state.amINext = value;
    },
    setTotalGameResult(state, value) {
      state.gameResultTotal.playerOnePoints = value.playerOne;
      state.gameResultTotal.playerTwoPoints = value.playerTwo;
    },
    startNewGame(state, value) {
      state.cardsForNewGame.playerOne = value.playerOne;
      state.cardsForNewGame.playerTwo = value.playerTwo;
      state.cardsForNewGame.playerOneCards = value.playerOneCards;
      state.cardsForNewGame.playerTwoCards = value.playerTwoCards;
      state.cardsForNewGame.userID = value.userID;
      state.cardsForNewGame.gameID = value.gameID;
    },
    getMatchResults(state, value) {
      state.matchResultPlayer1 = value.data.player_one_matches_result;
      state.matchResultPlayer2 = value.data.player_two_matches_result;
    },
    getFinalResultAPI(state, value) {
      console.log("valye", value);
      state.finalResult = value;
    }
  }
};
