/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

//support Vuex
import Vuex from "vuex";
Vue.use(Vuex);
import storeData from "./store/index";

// Buefy
import Buefy from "buefy";
import "buefy/dist/buefy.css";

Vue.use(Buefy);

const store = new Vuex.Store(storeData);

Vue.component(
  "chat-component",
  require("./components/ChatComponent.vue").default
);

/**
 * vue scroll pugin
 */
import VueChatScroll from "vue-chat-scroll";
Vue.use(VueChatScroll);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// DASHBOARD
Vue.component(
  "player-dashboard-data",
  require("./components/dashboard/player-dashboard-data.vue").default
);

Vue.component(
  "dashboard-navigation",
  require("./components/dashboard/dashboard-navigation.vue").default
);

Vue.component(
  "dashboard-available-players",
  require("./components/dashboard/dashboard-available-players.vue").default
);

Vue.component("tablic", require("./components/tablic/tablic.vue").default);

Vue.component(
  "example-component",
  require("./components/ExampleComponent.vue").default
);

Vue.component(
  "tablic-my-cards-component",
  require("./components/tablic/my-cards.vue").default
);
Vue.component(
  "tablic-table-cards-component",
  require("./components/tablic/table-cards.vue").default
);
Vue.component(
  "sidebar-available-users",
  require("./components/tablic/sidebar-available-users.vue").default
);

// SHARED
Vue.component(
  "start-game-button",
  require("./components/buttons/start-game-button.vue").default
);

Vue.component(
  "explanation-sidebar",
  require("./components/sidebar/sidebar.vue").default
);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
  el: "#app",
  store, //vuex
  data() {
    return {
      gameStarted: ""
    };
  },
  mounted() {
    /* catch event from sidebar component, and start game*/
    this.$root.$on("firstRound", async cards => {
      console.log("BLUZ BEBO", cards);
      this.gameStarted = await cards.data;
      this.$store.dispatch("toggleGameStart", cards.data);
    });
  }
});
