import { mount } from "@vue/test-utils";
import sinon from "sinon";

//import axios from "axios";
import Echo from "laravel-echo";

import StartGameButton from "./../components/buttons/start-game-button.vue";

jest.mock("axios", () => ({
  post: () =>
    Promise.resolve({
      data: {
        first_player: 1,
        second_player: 2,
        first_player_cards: {
          4: [14, "karo"],
          5: [12, "herc"],
          6: [5, "tref"],
          7: [1, "karo"],
          8: [1, "herc"],
          9: [2, "herc"]
        },
        second_player_cards: {
          10: [13, "pik"],
          11: [5, "pik"],
          12: [7, "herc"],
          13: [4, "karo"],
          14: [5, "herc"],
          15: [4, "tref"]
        },
        cards_on_table: [
          [12, "tref"],
          [10, "pik"],
          [2, "pik"],
          [12, "karo"]
        ],
        userID: 1,
        gameID: 45
      }
    })
}));

describe("start-game-button.vue", () => {
  test("is a Vue instance", () => {
    const wrapper = mount(StartGameButton, {
      propsData: {
        playerOne: "1",
        playerTwo: "2",
        user: {}
      }
    });
    expect(wrapper.isVueInstance).toBeTruthy();
  });

  test("after button click, activate startGame function", async () => {
    const wrapper = mount(StartGameButton, {
      propsData: {
        playerOne: "1",
        playerTwo: "2",
        user: {}
      }
    });
    const spy = sinon.spy(wrapper.vm, "startGame");

    const button = wrapper.find("button.btn.btn-success");

    expect(button.exists()).toBe(true);

    await button.trigger("click");
    expect(spy.calledOnce).toBe(true);
  });

  test("Start button should have proper title", () => {
    const wrapper = mount(StartGameButton, {
      propsData: {
        playerOne: "1",
        playerTwo: "2",
        user: {}
      }
    });

    const button = wrapper.find("button.btn");
    expect(button.text()).toBe("Start game!");
  });

  test("should fetch cards from an API endpoint", async () => {
    const wrapper = mount(StartGameButton, {
      propsData: {
        playerOne: "1",
        playerTwo: "2",
        user: {}
      }
    });
    const spy = sinon.spy(wrapper.vm, "startGame");

    const button = wrapper.find("button.btn");
    //await button.trigger("click");

    let apiResult = await spy().then(res => {
      return res.data;
    });
    //      console.log("res", res[0].val);
    expect(apiResult.first_player).toBe(1);
    expect(apiResult.second_player).toBe(2);
    expect(Object.keys(apiResult.first_player_cards).length).toBe(6);
    expect(Object.keys(apiResult.second_player_cards).length).toBe(6);
    expect(Object.keys(apiResult.cards_on_table).length).toBe(4);
    expect(apiResult.userID).toBe(1);
    expect(apiResult.gameID).toBe(45);
    //  await expect(axios.startGame("react")).resolves.toEqual(data);
  });
});
