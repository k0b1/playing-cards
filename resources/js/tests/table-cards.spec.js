import TableCards from "./../components/tablic/table-cards.vue";
import Vuex from "vuex";
import { mount, createLocalVue } from "@vue/test-utils";
// import api from "./../api/api";
import { cloneDeep } from "lodash";
import storeVuex from "./../store/index";

// ******
// imorted Echo and Pusher, check for better ways... also for Vuex setup
import Echo from "laravel-echo";
window.Pusher = require("pusher-js");
window.Echo = new Echo({
  broadcaster: "pusher",
  key: "af6489d88ce45ccc2b27",
  cluster: "eu",
  encrypted: true
});

// *******
// jest.mock("axios", () => ({
//   post: () =>
//     Promise.resolve({
//       data: {
//         first_player: 1,
//         second_player: 2,
//         first_player_cards: {
//           4: [14, "karo"],
//           5: [12, "herc"],
//           6: [5, "tref"],
//           7: [1, "karo"],
//           8: [1, "herc"],
//           9: [2, "herc"]
//         },
//         second_player_cards: {
//           10: [13, "pik"],
//           11: [5, "pik"],
//           12: [7, "herc"],
//           13: [4, "karo"],
//           14: [5, "herc"],
//           15: [4, "tref"]
//         },
//         cards_on_table: [
//           [12, "tref"],
//           [10, "pik"],
//           [2, "pik"],
//           [12, "karo"]
//         ],
//         userID: 1,
//         gameID: 45
//       }
//     })
// }));

const localVue = createLocalVue();
localVue.use(Vuex);

//localVue.use(Echo);
//localVue.use(window.Pusher);
localVue.use(window.Echo);
describe("table-cards.vue", () => {
  let store;

  beforeEach(() => {
    store = new Vuex.Store(cloneDeep(storeVuex));
  });
  test("is Vue instance", async () => {
    let wrapper = mount(TableCards, {
      propsData: {
        cardsOnTable: [],
        playerOne: "",
        playerTwo: "",
        currentPlayer: 1,
        gameID: 1,
        whoPlaysFirst: 0
      },
      store,
      Vuex,
      localVue
    });

    expect(wrapper.isVueInstance).toBeTruthy();
  });
  test("should add css class after click on card", async () => {
    let wrapper = mount(TableCards, {
      propsData: {
        cardsOnTable: [],
        playerOne: "",
        playerTwo: "",
        currentPlayer: 1,
        gameID: 1,
        whoPlaysFirst: 0
      },
      store,
      Vuex,
      localVue
    });
    await wrapper.setData({
      cards: [10, "pik"]
    });

    await wrapper.vm.$nextTick();
    const div = wrapper.find(".tablic-table-cards__card");
    await div.trigger("click");

    expect(div.classes("is-selected")).toBe(true);

    await div.trigger("click");
    expect(div.classes("is-selected")).toBe(false);
  });

  test("should get proper card values after click on it", async () => {
    let wrapper = mount(TableCards, {
      propsData: {
        cardsOnTable: [],
        playerOne: "",
        playerTwo: "",
        currentPlayer: 1,
        gameID: 1,
        whoPlaysFirst: 0
      },
      store,
      Vuex,
      localVue
    });
    await wrapper.setData({
      cards: [[10, "pik"]]
    });

    await wrapper.vm.$nextTick();
    const div = wrapper.find(".tablic-table-cards__card");
    await div.trigger("click");

    expect(wrapper.vm.selectedCardsValues[0]).toMatchObject([10, "pik"]);
    expect(wrapper.vm.selectedCardsPositions).toMatchObject([0]);
    expect(wrapper.vm.cardStrength).toBe(10);
  });

  test("on second click single card should reset card value", async () => {
    let wrapper = mount(TableCards, {
      propsData: {
        cardsOnTable: [],
        playerOne: "",
        playerTwo: "",
        currentPlayer: 1,
        gameID: 1,
        whoPlaysFirst: 0
      },
      store,
      Vuex,
      localVue
    });
    await wrapper.setData({
      cards: [[10, "pik"]]
    });

    await wrapper.vm.$nextTick();
    const div = wrapper.find("#card-0");
    await div.trigger("click");

    expect(wrapper.vm.cardStrength).toBe(10);

    await div.trigger("click");
    expect(wrapper.vm.cardStrength).toBe(-10);
  });

  test("should proper calculate sum of card values", async () => {
    let wrapper = mount(TableCards, {
      propsData: {
        cardsOnTable: [],
        playerOne: "",
        playerTwo: "",
        currentPlayer: 1,
        gameID: 1,
        whoPlaysFirst: 0
      },
      store,
      Vuex,
      localVue
    });
    await wrapper.setData({
      cards: [
        [10, "pik"],
        [2, "pik"]
      ]
    });

    await wrapper.vm.$nextTick();
    const cardOne = wrapper.find("#card-0");
    await cardOne.trigger("click");

    expect(wrapper.vm.cardStrength).toBe(10);

    const cardTwo = wrapper.find("#card-1");
    await cardTwo.trigger("click");
    //  await wrapper.vm.$store.getters.getGameStart;
    // console.log(wrapper.vm.selectedCardsValues);
    // expect(wrapper.vm.cardStrength).toBe(2);

    expect(
      wrapper.vm.selectedCardsValues[0][0] +
        wrapper.vm.selectedCardsValues[1][0]
    ).toBe(12);
  });

  test("should make selected card red", async () => {
    let wrapper = mount(TableCards, {
      propsData: {
        cardsOnTable: [],
        playerOne: "",
        playerTwo: "",
        currentPlayer: 1,
        gameID: 1,
        whoPlaysFirst: 0
      },
      store,
      Vuex,
      localVue
    });

    await wrapper.setData({
      cards: [[10, "pik"]]
    });

    const card = wrapper.find(".tablic-table-cards__card");
    await card.trigger("click");

    expect(card.classes()).toContain("is-selected");
  });
});
