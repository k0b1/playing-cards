import axios from "axios";

export default {
  startGame(playerOne, playerTwo) {
    return axios.post("/game/tablic/" + playerOne + "/" + playerTwo, {
      player1: playerOne,
      player2: playerTwo
    });
  },
  saveToDBWhoTookCards(currentPlayer, playerOne, playerTwo) {
    return axios.post("/who_took_cards", {
      current_player: currentPlayer,
      playerOne: playerOne,
      playerTwo: playerTwo
    });
  },
  increaseAdditionalPoints(
    playerOne,
    playerTwo,
    currentPlayer,
    additionalPoints
  ) {
    return axios.post("/additional-points", {
      player1: playerOne,
      player2: playerTwo,
      current_player: currentPlayer,
      additionalPoint: additionalPoints
    });
  },
  getAdditionalPoints(playerOne, playerTwo, currentPlayer) {
    return axios.post("/get-additional-points", {
      player1: playerOne,
      player2: playerTwo,
      current_player: currentPlayer
    });
  },
  deleteThrownCard(thrownCard, player1, player2) {
    axios
      .post("/delete-thrown-card", {
        card: thrownCard,
        player1: player1,
        player2: player2
      })
      .then(response => {});
  },
  saveResult(player1, player2, currentPlayer, result) {
    return axios.post("/saveTablicResult", {
      player1: player1,
      player2: player2,
      currentPlayer: currentPlayer,
      result: result
    });
  },
  setCardsOnTheTable(cards, player1, player2) {
    return axios.post("/tablic-logic", {
      cards: cards,
      player1: player1,
      player2: player2
    });
  },
  whoGetLastCardsOnTheTable(player1, player2) {
    return axios.post("/last_cards_on_table", {
      player1: player1,
      player2: player2
    });
  },
  async totalResultForPlayers(player1, player2, currentPlayer) {
    //    console.log("yo", player1, player2, currentPlayer);
    return axios
      .post("/get-current-total-result", {
        player1: player1,
        player2: player2,
        currentPlayer: currentPlayer
      })
      .catch(err => err);
  },

  setMatchResultIntoDB(player1, player2, currentPlayer, result_id) {
    return axios.post("/tablic__game_over", {
      player1: player1,
      player2: player2,
      currentPlayer: currentPlayer,
      result_id: result_id
    });
  },

  getMatchResultFromDB(player1, player2, currentPlayer) {
    return axios.post("/get_tablic_match_results", {
      player1: player1,
      player2: player2,
      currentPlayer: currentPlayer
    });
  },

  setGameResultIntoDB(player1, player2, currentPlayer) {
    return axios.post("/set_final_result", {
      player1: player1,
      player2: player2,
      currentPlayer: currentPlayer
    });
  },

  getGameResultFromDB(player1, player2, currentPlayer) {
    return axios.post("/get_final_result", {
      player1: player1,
      player2: player2,
      currentPlayer: currentPlayer
    });
  }
};
