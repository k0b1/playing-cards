@extends('layouts.app-test')

@section('title','Tablic fun')

@section('content')
  <div class="pc-tablic__container">
    <div v-if="$store.getters.getGameStart.gameID != 0" class="tablic__main-content">
      <tablic :cards-on-table="$store.getters.getGameStart.cards_on_table"
              :player-one="$store.getters.getGameStart.first_player"
              :player-two="$store.getters.getGameStart.second_player"
              :current-player="{{Auth::user()->id}}"
              :game-id="$store.getters.getGameStart.gameID"
              :who-plays-first="{{ $whoPlaysFirst }}"
              :current-player="{{$current_player}}"
      ></tablic>
    </div>
    <div v-else class="tablic__main-content">
      <h2 class="tablic__main-content__hello-world">
        Zdravo {{$first_player_name}} i {{$second_player_name}}
      </h2>
      <h2 class="tablic__main-content__start-game">
        Za pocetak pritisnite dugme 'Start game'!!!
      </h2>
      <start-game-button
        :user="{{ auth()->user() }}"
        :player-one="{{ json_encode($first_player) }}"
        :player-two="{{ json_encode($second_player) }}"
        :current-player="{{Auth::user()->id}}"
      ></start-game-button>
  </div>
  <div class="tablic__sidebar">
    <sidebar-available-users
      :user="{{ auth()->user() }}"
      :player-one="{{ json_encode($first_player) }}"
      :player-two="{{ json_encode($second_player) }}"
      :player-one-name="{{json_encode($first_player_name) }}"
      :player-two-name="{{json_encode($second_player_name) }}"
      :current-player="{{Auth::user()->id}}"
    ></sidebar-available-users>
  </div>
</div>
@endsection
