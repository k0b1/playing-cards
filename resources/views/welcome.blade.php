<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tablic</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <!-- Styles -->
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
      <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
          {{ config('app.name', 'Laravel') }}
        </a>
        @if (Route::has('login'))
          <div class="links">
            @auth
            <a href="{{ url('/home') }}">Dashboard</a>
        @else
            <a href="{{ route('login') }}">Login</a>

            @if (Route::has('register'))
              <a href="{{ route('register') }}">Register</a>
            @endif
            @endauth
          </div>
        @endif

      </div>
    </nav>
    <header>
      <div class="header__bg"></div>
      <h1 class="pc-homepage__intro-text-title">Playing Cards <br> version 0.1</h1>
    </header>
    <section>
      <div class="pc-homepage__intro-image-container">
        <img
          alt="playing-cards"
          src="images/homepage/cards.png" class="pc-homepage__intro-image" />
        
      </div>   
    </section>
    <section class="pc-homepage__bottom">
      <div class="pc-u-center pc-u-add-margin-top-5">
        <img alt="" src="images/homepage/pan-and-line." class="pc-homepage__section-separator" />
      </div>
      <div class="flex-center position-ref">     
        <div class="pc-main-page__container">
          <div class="pc-main-page__text">
            <div class="pc-homepage__intro-text container">  
            </div>
            <div class="container">
              <div class="row">
                <div class="col-sm pc-homepage__intro-column">
                  <h4>O tablicu</h4>
                  <p class="pc-homepage__intro-column-text">
                    Tablic je igra kartama popularna na prostoru bivse Jugoslavije. Moze da se igra u dvoje i u cetvoro.
                  </p>
                </div>
                <div class="col-sm pc-homepage__intro-column">
                  <h4>Sta?</h4>
                  <p class="pc-homepage__intro-column-text">
                    Na ovom sajtu trenutno mozete da igrate u dvoje, sa vasim izabranim saigracem/icom.
                  </p>
                </div>
                <div class="col-sm pc-homepage__intro-column">
                  <h4>Registrujte se!</h4>
                  <p class="pc-homepage__intro-column-text">
                    Potrebno je da se registrujete, kako bi mogli da pristupite igranju. Uzivajte!
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="pc-homepage__middle">

      <div class="pc-homepage__middle-background">
        <img alt="" src="images/homepage/cards-habd.jpg" class="pc-homepage__middle-image" />
      </div>
      <h2 class="pc-homepage__middle-text-title">Ovde ide neka tekstualna poruka</h2>
    </section>
    <section class="pc-homepage__main-message">
      <div class="container pc-u-center">
        <img alt="" src="images/homepage/pan-and-line.png" class="pc-homepage__section-separator" />
        <div class="row">
          <p class="justify-content-center is-text-centered pc-intro-text">
            Ova aplikacija napravljen je tokom pandemije Covid-19, namenjena je zabavi i tehnickom eksperimentu autora.
          </p>
        </div>
      </div>  
    </section>

    </body>
</html>
