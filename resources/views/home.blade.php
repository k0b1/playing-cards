@extends('layouts.app-test')

@section('content')
  <div class="pc-l-dashboard__container">
    <div class="pc-c-dashboard__main-title-container">
      <h1 class="pc-c-dashboard__main-title">Dashboard</h2>
      <h2 class="pc-c-dashboard__hello-user">Zdravo {{Auth::user()->name}}!</h2>   
    </div>
    
    <p class="pc-c-dashboard__options-text">
      Izaberi sa kim hoces da igras u sekciji "Izaberi saigraca", ili promeni svoje podatke u sekciji "Vasi podaci"
    </p>
    
    <div class="pc-l-dashboard__content">
      <div class="pc-c-dashboard__options">
        @if (session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
        @endif
        <dashboard-navigation
          username="{{Auth::user()->name}}"
          email="{{Auth::user()->email}}"
          :users="{{json_encode($users)}}"
          :current-user="{{json_encode($current_user)}}"
          :is-online="{{json_encode($is_user_online)}}"
        >
        </dashboard-navigation>
      </div>        
    </div>
  </div>
    
</div>
@endsection
