<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class HomeController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {
    $is_user_online = [];
      
    if (Auth::check()) {
      $user_is_logged_in = DB::table('users')
                         ->where('id', Auth::user()->id)
                    ->update(['is_logged_in' => 1]);
    }
    // get logged in user into Tablic
    $users = DB::table('users')->select('name', 'id')->get();
    
    foreach ($users as $user) {
        if ( Cache::has('user-is-online-' . $user->id)) {
            $is_user_online[$user->name] = true;
        }
    }
    
    // check if user is online

    
    return view('home', [
        "users" => $users,
        "current_user" => Auth::user()->id,
        "is_user_online" => $is_user_online ]);
  }
}
