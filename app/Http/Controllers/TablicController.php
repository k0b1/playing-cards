<?php

namespace App\Http\Controllers;

use App\Events\StartGameEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Game;
use App\Events\TablicEvent;
use App\helpers\CardsActions as Cards;

class TablicController extends Controller
{
  public function __construct() {
    $this->middleware('auth');  
  }

  const SPIL = 52;
  const BROJ_KARTI_IGRACU = 6;
  const IZBACI_NAKON_SECENJA = 4;
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */

  public function index($player1, $player2 )
  {
    $first_player = $this->player1 = $player1;
    $second_player = $this->player2 = $player2;

    $first_player_name = DB::table('users')
                       ->select('name')
                       ->where('id', '=', $first_player)
                       ->get();

    $second_player_name = DB::table('users')
                        ->select('name')
                        ->where('id', '=', $second_player)
                        ->get();
    
    $userID = Auth::user()->id;

    $who_plays_first = DB::table('games')
                     ->select('who_plays_first', 'who_played_first')
        ->where('player1', '=', $player1)
        ->where('player2', '=', $player2)
        ->get();

    if ( empty($who_plays_first[0]) ) {
        $who_plays_first = $player1;
    } else {
        $who_plays_first = $who_plays_first[0]->who_plays_first;
    }
     
    // if ($who_plays_first[0]->who_played_first == $player1) {
    //     $who_plays_next = $who_plays_first[0]->who_is_next;
    // } else if ( $who_plays_first[0]->who_played_first == "1" ) {
    //     $who_plays_next = $player1 == $userID ? $player1 : $player2;
    // }
   
        
    return view('/game/tablic', [
      'first_player' => $first_player,
      'second_player' => $second_player,
      'first_player_cards' => 'hello',
      'second_player_cards' => 'world',
      'cards_on_table' => '',
      'userID' => $userID,
      'first_player_name' => $first_player_name[0]->name,
      'second_player_name' => $second_player_name[0]->name,
      'current_player' => $userID,
      'whoPlaysFirst' => $who_plays_first
    ]);
  }

  public function start_game($player1, $player2 )
  {
    $cards = new Cards($player1, $player2, 52); 

    $cards->create_spil();
    $get_spil = $cards->get_spil($player1, $player2);

    $spil = $get_spil[0]->spil;

    $gameID = DB::table('games')
            ->select('id')
            ->where('player1', '=', $player1)
            ->where('player2', '=', $player2)
            ->get()->toArray();

    $userID = Auth::user()->id;

    $this->update_who_plays_next($player1, $player2, $userID);

    /* if game_started = 0 define who_plays_first; when game_started = 1, leave who_playes_first defined in previous step
       use DB:: select and define Vuex property based on who_plays_first

       if (game_started == 0) {
         $who_plays_next = $current_player;
       } else if ( game_started == 1 ) {
         $who_plays_next = $player1 == $current_player ? $player2 : $player1 
       }
     */
    $podeli = $cards->cards_first_round($spil);

    // broadcast code below was part of experiment to give cards to both players
    // after start button click, but I throwed away that idea, it didn't look
    // user friendly
    // broadcast( new StartGameEvent(
    //     [
    //         $podeli['prvi_igrac'],
    //         $podeli['drugi_igrac']
    //     ], $gameID)
    // );
    
    
    return response()->json([
       'first_player' => $player1,
       'second_player' => $player2,
       'first_player_cards' =>$podeli['prvi_igrac'],
       'second_player_cards' => $podeli['drugi_igrac'],
       'cards_on_table' => $podeli['cetiri_karte'],
       'userID' => $userID,
       'gameID' => $gameID[0]->id
       ]);
  }

  //finish this
  protected function update_who_plays_next($player1, $player2, $current_player) {
      // reminder, instead who_is_next, use who_plays_first, instead game_started use who_played_first
      // $game_started = DB::table('games')
      //               ->select('who_plays_first','who_played_first')
      //               ->where("player1", "=", $player1)
      //               ->where('player2', '=', $player2)
      //               ->get();

      /* I SHOULD PROBABLY DELETE who_played_first COLUMN FROM DATABASE */
          if ( $current_player == $player1 ) {
              // if player1 click twice in row on Start button, and player2 click neither
              DB::table('games')
                  ->where('player1', '=', $player1)
                  ->where('player2', '=', $player2)
                  ->update(['who_played_first' => $player1]);

              DB::table('games')
                  ->where('player1', '=', $player1)
                  ->where('player2', '=', $player2)
                  ->update(['who_plays_first' => $player1]);
          }

          if ( $current_player == $player2 ) {
              // if player1 click twice in row on Start button, and player2 click neither
              DB::table('games')
                  ->where('player1', '=', $player1)
                  ->where('player2', '=', $player2)
                  ->update(['who_played_first' => $player2]);

              DB::table('games')
                  ->where('player1', '=', $player1)
                  ->where('player2', '=', $player2)
                  ->update(['who_plays_first' => $player2]);
          }
      
      //  ako je drugo jedan i prvo jedan, u drugo upisi tri
      // ako je u prvom jedan i u drugom tri, u prvo upisi tri
      // ako je prvom tri i u drugom tri, u drugo upisi jedan
      // ako je u prvom tri a drugo jedan, onda i u prvo upisi jedan
      // if (
      //     $game_started[0]->who_plays_first == $player1
      //     && $game_started[0]->who_played_first == $player1
      // ) {
      //     //          $player1 == $current_player ? $player2 : $player1;
      //     if ( $current_player == $player1 ) {
      //         // if player1 click twice in row on Start button, and player2 click neither
      //         DB::table('games')
      //             ->where('player1', '=', $player1)
      //             ->where('player2', '=', $player2)
      //             ->update(['who_played_first' => $player1]);

      //         DB::table('games')
      //             ->where('player1', '=', $player1)
      //             ->where('player2', '=', $player2)
      //             ->update(['who_plays_first' => $player1]);
      //     } else {
      //         DB::table('games')
      //             ->where('player1', '=', $player1)
      //             ->where('player2', '=', $player2)
      //             ->update(['who_played_first' => $player2]);

      //         DB::table('games')
      //             ->where('player1', '=', $player1)
      //             ->where('player2', '=', $player2)
      //             ->update(['who_plays_first' => $player1]);
      //     }

      // }
      // else if (
      //     $game_started[0]->who_plays_first == $player1
      //     && $game_started[0]->who_played_first == $player2 ) {
      //     if ( $current_player == $player1 ) {
      //         DB::table('games')
      //             ->where('player1', '=', $player1)
      //             ->where('player2', '=', $player2)
      //             ->update(['who_played_first' => $player1]);

      //         DB::table('games')
      //             ->where('player1', '=', $player1)
      //             ->where('player2', '=', $player2)
      //             ->update(['who_plays_first' => $player1]);
      //     } else {
      //         DB::table('games')
      //              ->where('player1', '=', $player1)
      //              ->where('player2', '=', $player2)
      //              ->update(['who_played_first' => $player2]);

      //          DB::table('games')
      //              ->where('player1', '=', $player1)
      //              ->where('player2', '=', $player2)
      //              ->update(['who_plays_first' => $player2]);
      //     }

      // }
      // else if (
      //     $game_started[0]->who_plays_first == $player2
      //     && $game_started[0]->who_played_first == $player1 ) {
      //     if ( $current_player == $player1 ) {
      //         DB::table('games')
      //             ->where('player1', '=', $player1)
      //             ->where('player2', '=', $player2)
      //             ->update(['who_played_first' => $player1]);

      //         DB::table('games')
      //             ->where('player1', '=', $player1)
      //             ->where('player2', '=', $player2)
      //             ->update(['who_plays_first' => $player2]);
      //     } else {
      //         DB::table('games')
      //             ->where('player1', '=', $player1)
      //             ->where('player2', '=', $player2)
      //             ->update(['who_played_first' => $player1]);

      //         DB::table('games')
      //             ->where('player1', '=', $player1)
      //             ->where('player2', '=', $player2)
      //             ->update(['who_plays_first' => $player1]);
      //     }
       
      // }
      // else if (
      //     $game_started[0]->who_plays_first == $player2
      //     && $game_started[0]->who_played_first == $player2 ) {
      //     //          $who_plays_next = $player1 == $current_player ? $player2 : $player1;

      //     if ( $current_player == $player2 ) {
      //         DB::table('games')
      //             ->where('player1', '=', $player1)
      //             ->where('player2', '=', $player2)
      //             ->update(['who_played_first' => $player2]);

      //         DB::table('games')
      //             ->where('player1', '=', $player1)
      //             ->where('player2', '=', $player2)
      //             ->update(['who_plays_first' => $player2]);
      //     } else {
      //         DB::table('games')
      //             ->where('player1', '=', $player1)
      //             ->where('player2', '=', $player2)
      //             ->update(['who_played_first' => $player1]);

      //         DB::table('games')
      //             ->where('player1', '=', $player1)
      //             ->where('player2', '=', $player2)
      //             ->update(['who_plays_first' => $player2]);
      //     }
          
      // }

  }

  public function deleteThrownCard( Request $request ) {
      $card = $request->card;
      $player1 = $request->player1;
      $player2 = $request->player2;

      $get_rest_of_the_cards = DB::table('games')
                             ->select('spil')
                             ->where('player1', '=', $player1)
                             ->where('player2', '=', $player2)
                             ->get();

      $result_to_array = json_decode($get_rest_of_the_cards[0]->spil);
      for ( $i = 0; $i < count($result_to_array); $i++) {
         if ($card[0] == $result_to_array[$i][0] && $card[1] == $result_to_array[$i][1]) {
           array_splice($result_to_array, $i, 1);
         }
      }
      //update spil in database
      return count($result_to_array);
  }

  public function whoTookCards(Request $request){
        $active_player = $request->current_player;        
        $player1 = $request->playerOne;
        $player2 = $request->playerTwo;
        
        $save_player_who_took_cards = DB::table('games')
                                    ->where('player1', '=', $player1)
                                    ->where('player2', '=', $player2)
                                    ->update(['who_took_cards' => $active_player]);

        $get_player_who_took_cards = DB::table('games')
                                   ->select('who_took_cards')
                                   ->where('player1', '=', $player1)
                                   ->where('player2', '=', $player2)
                                   ->get();
        return $get_player_who_took_cards;
    }

  // private function get_current_user() {
  //   return Auth::user()->id;
  // }

  public function tablic_logic( Request $request) {
    $player1 = $request["player1"];
    $player2 = $request["player2"];
    $cards = $request["cards"];

    /* add card that each player throw to the cards on table  */ 
    $save_thrown_card = DB::table('games')
                      ->where('player1', '=', $player1)
                      ->where('player2', '=', $player2)
                      ->update(['cards_on_table' => $cards]);

        /* get current cards on the table  */ 
    $game = DB::table('games')
          ->select([
              'cards_on_table',
              'who_is_next',
              'player1_poens',
              'player2_poens',
              'player1_matches',
              'player2_matches',
              'result_id'
          ])
          ->where('player1', '=', $player1)
          ->where('player2', '=', $player2)
          ->get()->toArray();
    $gameID = DB::table('games')
            ->select('id')
            ->where('player1', '=', $player1)
            ->where('player2', '=', $player2)
            ->get()->toArray();

    broadcast(new TablicEvent(new Game($game), $gameID));

    return response()->json($game);
                                         
  }

  public function get_tablic_match_results( Request $request) {
    $player1 = $request->player1;
    $player2 = $request->player2;
    $current_player = $request->currentPlayer;
    
    $get_previous_game_player1 = DB::table('games')
                 ->select('player1_matches')
                 ->where('player1', '=', $player1)
                 ->where('player2', '=', $player2)
                 ->get();

    $get_previous_game_player2 = DB::table('games')
                 ->select('player2_matches')
                 ->where('player1', '=', $player1)
                 ->where('player2', '=', $player2)
                 ->get();
    if (empty($get_previous_game_player1)) {
        $previous_result_for_player_one =
                     ($get_previous_game_player1[0]->player1_matches == "")
                     ? 0
                     : $get_previous_game_player1[0]->player1_matches;
    } else {
        $previous_result_for_player_one = 0;
    }
    if (empty($get_previous_game_player2)) {
       $previous_result_for_player_two =
             ($get_previous_game_player2[0]->player2_matches == "")
             ? 0
             : $get_previous_game_player2[0]->player2_matches;
    } else {
        $previous_result_for_player_two = 0;
    }
    
    if ( $player1 == $current_player ) {
      $result_player_one = $previous_result_for_player_one;
    } else {
    $result_player_one = $previous_result_for_player_one;
    }

    if ( $player2 == $current_player ) {
      $result_player_two = $previous_result_for_player_two;
    } else {
      $result_player_two = $previous_result_for_player_two;
    }

    return [
        'player_one_matches_result' => $result_player_one,
        'player_two_matches_result' => $result_player_two    
    ];
  }

  public function whoTakesLastCardsOnTable( Request $request) {
      $player1 = $request->player1;
      $player2 = $request->player2;

      $who_was_last = DB::table('games')
                    ->select('who_took_cards', 'who_is_next')
                    ->where('player1', '=', $player1)
                    ->where('player2', '=', $player2)
                    ->get();
      return [$who_was_last[0]->who_took_cards, $who_was_last[0]->who_is_next ];
  }

  public function getNewCards(Request $request) {
  $player1 = $request["player1"];
  $player2 = $request["player2"];
  $current_user = $request['user'];
  $get_new_cards = DB::table('games')
                 ->select('spil')
                 ->where('player1', '=', $player1)
                 ->where('player2', '=', $player2)
                 ->get()->toArray();

  $get_who_is_next = DB::table('games')
                   ->select('who_is_next')
                   ->where('player1', '=', $player1)
                   ->where('player2', '=', $player2)
                   ->get()->toArray();

  // find cards for second round
  // $playerOneCards = [];
  // $playerTwoCards = [];
  $playerCards = [];
  $get_new_cards = json_decode($get_new_cards[0]->spil);
  $get_who_is_next = json_decode($get_who_is_next[0]->who_is_next );


  // THERE IS give_cards METHOD ABOVE

  foreach( $get_new_cards as $card_position => $card_value ) {
      if ($card_position >=16 && $card_position < 22 && $get_who_is_next == 0  ) {
          array_push($playerCards, $card_value);
      }
      if ($card_position >=22 && $card_position < 28 && $get_who_is_next == 1 ) {
          array_push($playerCards, $card_value);
      }
      if ($card_position >=28 && $card_position < 34 && $get_who_is_next == 2 ) {
          array_push($playerCards, $card_value);
      }
      if ($card_position >=34 && $card_position < 40 && $get_who_is_next == 3 ) {
          array_push($playerCards, $card_value);
      }                    
      if ($card_position >=40 && $card_position < 46 && $get_who_is_next == 4 ) {
          array_push($playerCards, $card_value);
      }
      if ($card_position >=46 && $card_position < 52 && $get_who_is_next == 5 ) {
          array_push($playerCards, $card_value);
      }

  }
  // probably not needed anymore
  // if ( $get_who_is_next == 6 ) return response()->json([['no more cards'], $current_user, $get_who_is_next]);

   $userName = Auth::user()->name;

   $game = DB::table('games')
         ->select('cards_on_table')
         ->where('player1', '=', $player1)
         ->where('player2', '=', $player2)
         ->get()->toArray();

   $update_who_is_next = DB::table('games')
                       ->where('player1', '=', $player1)
                       ->where('player2', '=', $player2)
                       ->update(['who_is_next' => $get_who_is_next+1 ]);

   return response()->json([$playerCards, $current_user, $get_who_is_next]);
}

  public function setAdditionalPoints(Request $request) {
      $player1 = $request->player1;
      $player2 = $request->player2;
      $current_player = $request->current_player;
      $additional_point = $request->additionalPoint;
      $points_in_db = "";
      if ( $current_player == $player1) {
        $get_additional_points = DB::table('games')
                              ->select('player_one_additional_points')
                              ->where('player1', '=', $player1)
                              ->where('player2', '=', $player2)
                              ->get();
        $points_in_db = $get_additional_points[0]->player_one_additional_points;

      } else if ( $current_player == $player2 ) {
         $get_additional_points = DB::table('games')
                              ->select('player_two_additional_points')
                              ->where('player1', '=', $player1)
                              ->where('player2', '=', $player2)
                              ->get();
         $points_in_db = $get_additional_points[0]->player_two_additional_points;

      }

      $additional_points = ( $points_in_db == "") ? 1 : $points_in_db + $additional_point;

      if ($current_player == $player1) {
        $update_additional_points = DB::table('games')
                            ->where('player1', '=', $player1)
                            ->where('player2', '=', $player2)
                            ->update(['player_one_additional_points' => $additional_points]);
      } else if ($current_player == $player2) {
        $update_additional_points = DB::table('games')
                            ->where('player1', '=', $player1)
                            ->where('player2', '=', $player2)
                            ->update(['player_two_additional_points' => $additional_points]);
      }

      if ($current_player == $player1) {
        $get_updated_additional_points = DB::table('games')
                              ->select('player_one_additional_points')
                              ->where('player1', '=', $player1)
                              ->where('player2', '=', $player2)
                              ->get();
      } else if ($current_player == $player2) {
        $get_updated_additional_points = DB::table('games')
                              ->select('player_two_additional_points')
                              ->where('player1', '=', $player1)
                              ->where('player2', '=', $player2)
                              ->get();
      }
      return $get_updated_additional_points;
  }

  public function getAdditionalPoints(Request $request) {
      $player1 = $request->player1;
      $player2 = $request->player2;
      $current_player = $request->current_player;
      if ( $current_player == $player1 ) {
        $get_additional_points = DB::table('games')
                              ->select('player_one_additional_points')
                              ->where('player1', '=', $player1)
                              ->where('player2', '=', $player2)
                              ->get();

      } else if ( $current_player == $player2 ) {
        $get_additional_points = DB::table('games')
                              ->select('player_two_additional_points')
                              ->where('player1', '=', $player1)
                              ->where('player2', '=', $player2)
                              ->get();
      }

      return $get_additional_points;
  }

  public function saveTablicResult( Request $request ) {
      $player1 = (int) $request->player1; // player1 is player that made first move in the game
      $player2 = (int) $request->player2; // player2 is player that made second move in the game
      $currentPlayer = (int) $request->currentPlayer;
      $result = $request->result;
      
      // get current result      
      if ( $player1 == $currentPlayer && $player1 < $player2 ) {
          $result_column = 'player1_poens';
          $get_result = DB::table('games')
             ->select($result_column)
             ->where('player1', '=', $player1)
             ->where('player2', '=', $player2)
             ->get();
          $updated_result = $this->updateResultForPlayers(
              $player1,
              $player2,
              $currentPlayer,
              "player1_poens",
              $result
          );
      }
      
      if ( $player2 == $currentPlayer && $player1 < $player2 ) {
          $result_column = "player2_poens";
          $get_result = DB::table('games')
             ->select($result_column)
             ->where('player1', '=', $player1)
             ->where('player2', '=', $player2)
             ->get();
          $updated_result = $this->updateResultForPlayers(
              $player1,
              $player2,
              $currentPlayer,
              "player2_poens",
              $result
          );
      }
      if ( $player1 == $currentPlayer && $player1 > $player2 ) {
          $result_column = "player2_poens";
          $get_result = DB::table('games')
             ->select($result_column)
             ->where('player1', '=', $player2)
             ->where('player2', '=', $player1)
             ->get();
          $updated_result = $this->updateResultForPlayers(
              $player2,
              $player1,
              $currentPlayer,
              "player2_poens",
              $result
          );
      }
      if ( $player2 == $currentPlayer && $player1 > $player2 ) {
          $result_column = "player1_poens";
          $get_result = DB::table('games')
             ->select($result_column)
             ->where('player1', '=', $player2)
             ->where('player2', '=', $player1)
             ->get();
          $updated_result = $this->updateResultForPlayers(
              $player2,
              $player1,
              $currentPlayer,
              "player1_poens",
              $result
          );
      }
      return $updated_result[0]->$result_column;
  }

  private function updateResultForPlayers(
      $playedFirst,
      $playedSecond,
      $currentPlayer,
      $result_column,
      $result
  ) {
      // get
      $get_result = DB::table('games')
         ->select($result_column)
         ->where('player1', '=', $playedFirst)
         ->where('player2', '=', $playedSecond)
         ->get();
      // update
      $previous_result =
                      ($get_result[0]->$result_column == "")
                      ? 0
                      : $get_result[0]->$result_column; // could add $result_column?

      $result_player = $result + $previous_result;

      $save_result = DB::table('games')
                 ->where('player1', '=', $playedFirst)
                 ->where('player2', '=', $playedSecond)
                 ->update([$result_column => $result_player]);

      // get updated
      $updated_result = DB::table('games')
                  ->select($result_column)
                  ->where('player1', '=', $playedFirst)
                  ->where('player2', '=', $playedSecond)
                  ->get();
      return $updated_result;
  }
  public function getCurrentTotalResult(Request $request) {
      $player1 = $request->player1;
      $player2 = $request->player2;
      $current_player = $request->currentPlayer;
      $result_column =
                     ($player1 == $current_player)
                     ? 'player1_poens'
                     : 'player2_poens';

      $get_result = DB::table('games')
                  ->select($result_column, 'player1_poens', 'player2_poens',)
                   ->where('player1', '=', $player1)
                   ->where('player2', '=', $player2)
                   ->get();
      if ( empty($get_result[0]) ) {
          $get_result = 0;
      } else {
          $get_result = $get_result[0];

      }
      return json_encode($get_result);
  }

  protected function setFinalResult(Request $request)
    {
        $player1 = (int)$request->player1;
        $player2 = (int)$request->player2;
        $current_player = $request->currentPlayer;
        if ( $current_player > $player1 ) {
            $column = "final_result_player2";
            $get_final_result = DB::table('games')
               ->select($column)
               ->where('player1', '=', $player1)
               ->where('player2', '=', $player2)
               ->get(); 
        } else if ($current_player <= $player1){
            $column = "final_result_player1";
            $get_final_result = DB::table('games')
               ->select($column)
               ->where('player1', '=', $player1)
               ->where('player2', '=', $player2)
               ->get(); 
        }
        $result = (int) $get_final_result[0]->$column;

        DB::table('games')
               ->where('player1', '=', $player1)
               ->where('player2', '=', $player2)
               ->update([
                   $column => $result + 1]);
        
        return [$get_final_result, $column, $player1, $player2, $current_player];
    }

  protected function getFinalResult(Request $request)
    {
        $player1 = (int)$request->player1;
        $player2 = (int)$request->player2;
        //$current_player = $request->currentPlayer;
        $get_final_result = DB::table('games')
               ->select("final_result_player1", "final_result_player2")
               ->where('player1', '=', $player1)
               ->where('player2', '=', $player2)
               ->get(); 
        return [
            $get_final_result[0]->final_result_player1,
            $get_final_result[0]->final_result_player2
        ];
    }

  public function tablic__game_over( Request $request ) {
      // write to database who won the match -> won 101 point
      $player1 = $request->player1;
      $player2 = $request->player2;
      $current_player = $request->currentPlayer;
      $who_won = $request->result_id == 0 ? 0 : $current_player;

      $get_previous_game_player1 = DB::table('games')
                   ->select('player1_matches')
                   ->where('player1', '=', $player1)
                   ->where('player2', '=', $player2)
                   ->get();

      $get_previous_game_player2 = DB::table('games')
                   ->select('player2_matches')
                   ->where('player1', '=', $player1)
                   ->where('player2', '=', $player2)
                   ->get();

      $previous_result_for_player_one =
                       ($get_previous_game_player1[0]->player1_matches == "")
                       ? 0
                       : $get_previous_game_player1[0]->player1_matches;

      $previous_result_for_player_two =
               ($get_previous_game_player2[0]->player2_matches == "")
               ? 0
               : $get_previous_game_player2[0]->player2_matches;

      if ( $player1 == $current_player ) {
        $result_player_one = $previous_result_for_player_one + 1;
      } else {
      $result_player_one = $previous_result_for_player_one;
      }

      if ( $player2 == $current_player ) {
        $result_player_two = $previous_result_for_player_two + 1;
      } else {
        $result_player_two = $previous_result_for_player_two;
      }

      $save_match_to_database = DB::table('games')
               ->where('player1', '=', $player1)
               ->where('player2', '=', $player2)
               ->update(['player1_poens' => 0,'player1_matches' => $result_player_one, 'result_id' => $who_won]);

      $save_match_to_database = DB::table('games')
               ->where('player1', '=', $player1)
               ->where('player2', '=', $player2)
               ->update(['player2_poens' => 0,'player2_matches' => $result_player_two]);
      return "helloe world". $result_player_one. ' rr '. $current_player. " glg " . $result_player_two . " who won?  ". $who_won. " preciznije" . $save_match_to_database;
    }
}
