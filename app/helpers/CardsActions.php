<?php
namespace App\helpers;

use Illuminate\Support\Facades\DB;

class CardsActions {

    //private const CARDS = 0;
    const CARDS_TO_PLAYER = 6;
    const CARDS_ON_TABLE = 4;
    const NUMBER_OF_CARDS = 52;
    
    private $give_cards_on_table = [];
    private $give_cards_to_first_player = [];
    private $give_cards_to_second_player = [];

    public $player1;
    public $player2;

    public function __construct($player1, $player2, $cards) {
        $this->player1 = $player1;
        $this->player2 = $player2;
        $this->CARDS = $cards;
    }

    public function create_spil() {
       $spil = [];
       $value = 0;
       for ( $i = 1; $i <= self::NUMBER_OF_CARDS; $i++ ) {
         if ( $i >= 1 && $i < 14) { // set card type
             $value = $value < 14 ? $value++ : 0;
             if ($value == 10 ) $value = 11;
             $spil[$i] = [++$value, "tref"];
         }
         if ( $i >= 14 && $i < 27) { // set card type
              if ($value == 10 ) $value = 11; 
             $value = $value < 14 ? $value++ : 0;
             $spil[$i] = [++$value, "herc"];
         }
         if ( $i >= 27 && $i < 40) { // set card type
             if ( $value == 10 ) $value = 11;
             $value = $value < 14 ? $value++ : 0;
             $spil[$i] = [++$value, "pik"];
         }
         if ( $i >= 40 && $i < 53) { // set card type
             if ( $value == 10 ) $value = 11;
             $value = $value < 14 ? $value++ : 0;
             $spil[$i] = [++$value, "karo"];
         }
       }

       shuffle( $spil ); // promesaj
       $this->save_spil($spil, $this->player1, $this->player2);
       return 0;    
  }

    private function save_spil($spil, $player1, $player2) {
      // check if game is already there
      $query = DB::table('games')
             ->select( 'id', 'spil', 'who_is_next')
             ->where('player1', '=', $player1)
             ->where('player2', '=', $player2)
             ->exists();
      
      if ( !$query ) {
        //save spil to database
        DB::table('games')->insert([
          'player1' => $player1,
          'player2' => $player2,
          'spil' => json_encode($spil),
          'cards_on_table' => '',
          'who_is_next'=> 0,
          'player1_poens' => '0',
          'player2_poens' => '0',
          'result_id' => '0',
          'who_took_cards' => '',
          'player_one_additional_points' => '0',
          'player_two_additional_points' => '0',
          'who_plays_first' => $player1,
          'who_played_first' => $player1
        ]);
      } else  {
          $game = DB::table('games')->select('id', 'who_is_next')->where('player1', '=', $player1)->where('player2', '=', $player2)->get();
          if ( $game[0]->who_is_next >= 6 ) {
               DB::table('games')
                ->where('player1', $player1)
                ->where('player2', $player2)
                ->update([
                  'player1' => $player1,
                  'player2' => $player2,
                  'spil' => json_encode($spil),
                  'cards_on_table' => '',
                  'who_is_next'=> 0,
                  'who_took_cards' => '',
                  'who_played_first' => $player1
                ]);
              }
      }
  }
  
    public function cards_first_round($spil) {
      /* $spil = $this->create_and_save_spil($player1, $player2); */

      $this->give_cards_on_table =
        $this->give_cards( $spil, $counter = 0, self::CARDS_ON_TABLE );

      $this->give_cards_to_first_player =
        $this->give_cards( $spil, $counter = 1, self::CARDS_TO_PLAYER );

      $this->give_cards_to_second_player =
        $this->give_cards( $spil, $counter = 2, self::CARDS_TO_PLAYER );

      $spil = json_decode($spil);

      return [
        'cetiri_karte' => $this->give_cards_on_table,
        'prvi_igrac' => $this->give_cards_to_first_player,
        'drugi_igrac' => $this->give_cards_to_second_player
      ];
    }

    private function give_cards( $spil, $counter, $how_many ) {
      $spil = json_decode( $spil, true );

      if ( $counter == 0 ) {
        for ( $i = 0; $i <= 3; $i++ ) {
          $podeli[$i] = $spil[$i];
          unset( $spil[$i] );// delete element
        }
      } else if ( $counter == 1 ) {
        for ( $i = 4; $i <= 9; $i++ ) {
          $podeli[$i] = $spil[$i];
          unset( $spil[$i] );// delete element
        } 
      } else if ( $counter == 2 ) {
        for ( $i = 10; $i <= 15; $i++ ) {
          $podeli[$i] = $spil[$i];
          unset( $spil[$i] );// delete element
        }
      }
      // update spil colummn in db, with $podeli
      $update_spil = 0;
      return $podeli;
  }

    public function get_spil( $player1, $player2 ) {
      $query = DB::table('games')->select('spil')->where('player1', '=', $player1)->where('player2', '=', $player2)->get();
      return $query;
    }
}
?>
