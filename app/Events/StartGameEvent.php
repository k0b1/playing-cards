<?php

namespace App\Events;

use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use App\Game;
//use App\Game;
//use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
//use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
//use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;


class StartGameEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $who_plays_first;
    public $gameID;
    
    public function __construct($data, $game_id)
    {
        $this->who_plays_first = $data;
        $this->gameID = $game_id;
    }

     public function broadcastOn()
    {
        //        return ['game'];
        return new PrivateChannel('gameStart.' . $this->gameID[0]->id);
    }
}
