<?php

use Illuminate\Support\Facades\Broadcast;
use App\Game;
use Illuminate\Support\Facades\DB;
/*
   |--------------------------------------------------------------------------
   | Broadcast Channels
   |--------------------------------------------------------------------------
   |
   | Here you may register all of the event broadcasting channels that your
   | application supports. The given channel authorization callbacks are
   | used to check if an authenticated user can listen to the channel.
   |
 */

Broadcast::channel('chat', function ($user) {  
  return $user;
});


/*
*  Register channel for "listening" of actions on card table, like
*  throwing cards on table, or taking cards of table.
*/
// Broadcast::channel('game', function ($game) {  
//   return $game;
// });

Broadcast::channel('game.{gameID}', function ($user, $gameID) {
    $player1 = Game::select('player1')->where('id', '=', $gameID)->get();
    $player2 = Game::select('player2')->where('id', '=', $gameID)->get();
    return $user->id == Game::find($gameID)->player1 || $user->id == Game::find($gameID)->player2;
});

Broadcast::channel('gameStart.{gameID}', function ($user, $gameID) {
    $player1 = Game::select('player1')->where('id', '=', $gameID)->get();
    $player2 = Game::select('player2')->where('id', '=', $gameID)->get();
    return $user->id == Game::find($gameID)->player1 || $user->id == Game::find($gameID)->player2;
});


Broadcast::channel('App.User.{id}', function ($user, $id) {
  return (int) $user->id === (int) $id;
});
