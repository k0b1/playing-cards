<?php

use Illuminate\Support\Facades\Route;

/*
   |--------------------------------------------------------------------------
   | Web Routes
   |--------------------------------------------------------------------------
   |
   | Here is where you can register web routes for your application. These
   | routes are loaded by the RouteServiceProvider within a group which
   | contains the "web" middleware group. Now create something great!
   |
 */

Route::get('/', function () {
  return view('welcome');
});

Auth::routes();

/* 
  |
  | Chat routes  
  |
  |
*/
Route::get('/chats', 'ChatController@index');
Route::get('/messages', 'ChatController@fetchAllMessages');
Route::post('/messages', 'ChatController@sendMessage');

// Broadcast::routes(['middleware' => 'auth']);
//Broadcast::routes(['middleware' => ['auth:api']]);

Route::post('/tablic-logic', 'TablicController@tablic_logic');
Route::get('cards_on_table', 'TablicController@cardsOnTable');
Route::post('/get-new-cards', 'TablicController@getNewCards');
Route::post('/delete-thrown-card', 'TablicController@deleteThrownCard');


Route::post('throw_card', 'TablicController@throwCard');
Route::post('who_took_cards', 'TablicController@whoTookCards');
Route::post('last_cards_on_table', 'TablicController@whoTakesLastCardsOnTable' );
Route::post('additional-points', 'TablicController@setAdditionalPoints');
Route::post('get-additional-points', 'TablicController@getAdditionalPoints');

Route::post('saveTablicResult', 'TablicController@saveTablicResult');
Route::post('get-current-total-result', 'TablicController@getCurrentTotalResult');
Route::post('tablic__game_over', 'TablicController@tablic__game_over');
Route::post('get_tablic_match_results', 'TablicController@get_tablic_match_results');
// Route::post('get_total_result_for_players', 'TablicController@get_total_result_for_players')

//Route::get('/get_cards_on_table', 'TablicController@get_cards_on_table');
Route::post('set_final_result', 'TablicController@setFinalResult');
Route::post('get_final_result', 'TablicController@getFinalResult');

/* 
  |
  | Card-actions routes  
  |
  |
*/


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/game/tablic/{player1}/{player2}', 'TablicController@index');
Route::post('/game/tablic/{player1}/{player2}/', 'TablicController@start_game');


Route::get('/cards-on-table/', 'TablicController@init')->name('card_on_table');


// Route::get('login', '\App\Http\Controllers\Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');


Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

Route::get('/status', 'UserController@userOnlineStatus');
