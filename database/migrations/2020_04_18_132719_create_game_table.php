<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('game', function (Blueprint $table) {
      $table->id();
      $table->string('player1');
      $table->string('player2');
      $table->json('spil'); // current spil
      $table->string('cards_on_table');
      $table->string('player1_poens');
      $table->string('player2_poens');
      $table->integer('result_id')->unsigned()->index();
      $table->foreign('result_id')->references('id')->on('results');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('game');
  }
}
